const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productControllers = require("../controllers/productControllers.js");

//Create product (Admin only)

router.post("/", auth.verify, (req,res) => {

	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin){
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}else{
		return res.send("User is not an Admin");
	}
})
//All product
router.get("/all", (req,res) => {
	productControllers.allProduct().then(resultFromController => res.send(resultFromController));
})


//Get all active product
router.get("/active", (req,res) => {
	productControllers.activeProduct().then(resultFromController => res.send(resultFromController));
})


//All inactive products
router.get("/inactive", (req,res) => {
	productControllers.inactiveProduct().then(resultFromController => res.send(resultFromController));
})

//Retrieve specific product
router.get("/:productId", (req,res) => {
	productControllers.singleProduct(req.params).then(resultFromController => res.send(resultFromController));
})

//Update product information(Admin only)
router.put("/:productId", auth.verify, (req,res) => {

	let data = auth.decode(req.headers.authorization);
	
	if(data.isAdmin){
		productControllers.updateProduct(req.body,req.params).then(resultFromController => res.send(resultFromController));
	}else{
		return false;
	}
})

//Archiving products
router.put("/archived/:productId", auth.verify, (req,res) => {

	if(auth.decode(req.headers.authorization).isAdmin){

		productControllers.archivedProduct(req.body,req.params).then(resultFromController => res.send(resultFromController));
	}else{
		return false;
	}
})

//All products
router.get("/all-product", (req,res) => {
	productControllers.allProducts().then(resultFromController => res.send(resultFromController));
})


//Product price
router.get("/price", (req,res) => {
	productControllers.productPrice().then(resultFromController => res.send(resultFromController));
})




//export the router objecto for index.js file
module.exports = router;