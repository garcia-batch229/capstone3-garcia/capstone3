const Product = require("../models/Product.js");
const product = require("../routes//product.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");


//Create a new product
/*
	1. Create a new product object using mongoose model
	2. Check is productName is already used
	3. Save the new product to the database
*/
module.exports.addProduct = (reqBody) => {
	return Product.findOne({productName:{$regex:reqBody.productName,$options: reqBody.productName}}).then((result) => {
		if(result != null){
			return [false, "Product name already used"];
		}else{
			let newProduct = new Product({
				productName: reqBody.productName,
				description: reqBody.description,
				price: reqBody.price
			})
			return newProduct.save().then((result,error) => {
				if(error){
					return false;
				}else{
					return [true, "Product successfully added!"];
				}
			})
		}
	})
}
//All products
module.exports.allProduct = () => {
	return Product.find().then(result => {
		return result;
	})
}




//All active products
/*
	1. Find all active products
*/

module.exports.activeProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

//All inactive products
/*
	1. Find all inactive products
*/
module.exports.inactiveProduct = () => {
	return Product.find({isActive: false}).then(result => {
		return result;
	})
}

//Retrieve specific product
/*
	1. Find single product
	2. A GET request is sent to the given endpoint
*/

module.exports.singleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then((result) => {
		return result;
	})
}

//Update product details with authorization
/*
	1. Create a variable "updateProduct" which will contain the info retrieved from the req.body
	2. Find and update the result
*/

module.exports.updateProduct = (reqBody,reqParams) => {
	
		let updatedProduct = {
			productName: reqBody.productName,
			description: reqBody.description,
			price: reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result,error) => {
			if(error){
				return [false, "Product ID does not exist"];
			}else{
				return [true, "Product details has been successfully updated"];
			}
		})
}



module.exports.archivedProduct = (reqBody, reqParams) => {

	let archivedProduct = {
		isActive: reqBody.isActive
	}


	return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((result,error) => {
		console.log(archivedProduct);

		if(error){
			return false;
		}else{
			
			return true;
		}
	})
}

//Get/Show all products

module.exports.allProducts = () =>{
	return Product.find({}).then(result => {
		return result;
	})
}


//Product Price
module.exports.productPrice = () => {

	return Product.find({price:{$gt:1}},{_id:0,productName:1,price:1,orders:1,orderId:1}).then(result => {
		

		return result;
	})
}