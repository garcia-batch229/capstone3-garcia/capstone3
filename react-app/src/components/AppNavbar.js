import {useContext, useEffect} from 'react';
import WebFont from 'webfontloader';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js';
export default function AppNavbar(){

	//const [user, setUser] = useState(localStorage.getItem('email'));

useEffect(() => {
  WebFont.load({
    google: {
      families: ['Raleway', 'Chilanka'] 
    }
  });
 }, []);

	const {user} = useContext(UserContext);

console.log(user.id);
	return (
		<Navbar  expand="lg" className="p-3 navBar">
			<Navbar.Brand className="p-3 text-light navBrand" hidden>Djan's Cakery</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto text-light">
					<Nav.Link className="text-light" as={NavLink} exact to="/" >Home</Nav.Link>
					<Nav.Link className="text-light" as={NavLink} exact to="/products">Product</Nav.Link>
						
					{

						(user.id !== null) ? 
						
						<Nav.Link className="text-light" as={NavLink} exact to="/logout">Logout</Nav.Link>
						:
						<>
						<Nav.Link className="text-light" as={NavLink} exact to="/login">Login</Nav.Link>
						<Nav.Link className="text-light" as={NavLink} exact to="/register">Register</Nav.Link>	
						
						</>
					}
					{
						(user.id === null || user.isAdmin === false) ?  
						<Nav.Link className="text-light" as={NavLink} exact to="/admin" hidden>Admin</Nav.Link>
						:
						<Nav.Link className="text-light" as={NavLink} exact to="/admin">Admin</Nav.Link>

					}
					{
						(user.id === null || user.isAdmin === true) ?  
						<Nav.Link className="text-light" as={NavLink} exact to="/myOrder" hidden>MyOrder</Nav.Link>
						:
						<Nav.Link className="text-light" as={NavLink} exact to={`/myOrder/${user.id}`}>MyOrder</Nav.Link>
					}



				</Nav>
			</Navbar.Collapse>

		</Navbar>



		)
}



							
