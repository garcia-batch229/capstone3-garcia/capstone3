// import Courses from './pages/Courses.js';

import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams,  useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2'
   
export default function CourseView(){

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	//THe "useParams hook allows us to retrieve the courseId passed via URL"
	const {courseId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(courseId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${courseId}`)
		.then(res => res.json())
		.then(data => {

		console.log(data);

		setName(data.productName);
		setDescription(data.description);
		setPrice(data.price);

		});
	}, [])


	const enroll = (courseId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: courseId,
				totalAmount: price
				
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: 'Order Received',
					icon: 'success',
					text: 'You have successfully placed your order'
				})

					// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component

				navigate("/products");

			}else{
				Swal.fire({
					title: 'Admin not allowed',
					icon: 'error',
					text: 'Please try again'
				})
			}

		});

	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							
							
							{
								(user.id !== null) ?
								<Button onClick={() => enroll(courseId)} variant="primary" block>Buy</Button>
								:
								<Link className="btn btn-danger btn-block" to="/login">Login</Link>
							}

							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

