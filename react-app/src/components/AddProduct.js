import {useState, useEffect} from 'react'
import { Link} from 'react-router-dom'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2';


export default function AddProduct() {

	

	const [productName, setProductName] = useState('');
	const [productDescription, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);


	useEffect(() => {
		if(productName !== '' && productDescription !== '' && price!== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[productName, productDescription, price])


	function addProducts(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
				
			},
			body: JSON.stringify({
				productName: productName,
				description: productDescription,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			if(data){
				Swal.fire({
                    title: "Product Successfully Added!",
                    icon: "success",
                    text: "You may now check on product list"
                });
                setProductName('');
                setDescription('');
                setPrice('');
                

			}
			else{
				Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
			}
		})
	}

	let url = window.location.href;
	//console.log(url);
	let getPrefixBySlash = url.split('/');
	//console.log(getPrefixBySlash[4]);

	return (
<>
			{
				(getPrefixBySlash[4] === 'addProduct') ?
				<Link  to="/admin">Return</Link>
				:
				<Link  to="/admin" hidden>Return</Link>
			}


		{	(getPrefixBySlash[4] === 'addProduct') ?
            
            <Form className="text-light" onSubmit={e => addProducts(e)}>
            <h1 className="my-5 text-center">Add Product</h1>
            <Form.Group className="mb-3" controlId="pName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter product name"
                    value={productName}
                    onChange={e => setProductName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter description"
                    value={productDescription}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter price"
                    onChange={e => setPrice(e.target.value)}
                    value={price}
                    required
                />
                
            </Form.Group>

            {
                isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Add product
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Add product
                    </Button>
            }

            </Form>
        :
            <Form className="text-light" onSubmit={e => addProducts(e)} hidden>
            <h1 className="my-5 text-center">Add Product</h1>
            <Form.Group className="mb-3" controlId="pName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter product name"
                    value={productName}
                    onChange={e => setProductName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter description"
                    value={productDescription}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter price"
                    onChange={e => setPrice(e.target.value)}
                    value={price}
                    required
                />
                
            </Form.Group>

            {
                isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Add product
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Add product
                    </Button>
            }

            </Form>

        }
        </>


		)


}