
import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';
import {useParams,  useNavigate, Navigate} from 'react-router-dom';

import Swal from 'sweetalert2'


export default function IsActive() {
 	
	const navigate = useNavigate();
	const {productId} = useParams();

	const [isActive, setIsActive] = useState(Boolean);

	useEffect(() => {
		

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/`)
		.then(res => res.json())
		.then(data => {

		console.log(data);
		setIsActive(data.isActive);
		})
	},[])


	function isActiveProduct(e){
		e.preventDefault();

		if(isActive == true){
			fetch(`${ process.env.REACT_APP_API_URL }/products/archived/${productId}`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
				
			},
			body: JSON.stringify({
				isActive: false
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
                    title: "Deactivation Completed",
                    icon: "success",
                    text: "You may now check on product list"
                });
                
                navigate('/admin');

			}
			else{
				Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
			}
		})	

		}else {
			fetch(`${ process.env.REACT_APP_API_URL }/products/archived/${productId}`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
				
			},
			body: JSON.stringify({
				isActive: true
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
                    title: "Reactivation Completed",
                    icon: "success",
                    text: "You may now check on product list"
                });
                
                navigate('/admin');

			}
			else{
				Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
			}
		})	
		}

		
	}


	return (

		<>
		<Form className="m-5"onSubmit={e => isActiveProduct(e)}>
            
            {
            	(isActive) ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Disable
                    </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn">
                    Enable
                    </Button>
            }


            
        </Form>
		
        </>
		)
}