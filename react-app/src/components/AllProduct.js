import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function AllProduct({productProps}){

	const {productName, description, price, isActive, _id } = productProps;

	console.log(isActive);

	return(
		<Card className="mx-4 ">
		    <Card.Body>
		        <Card.Title className="mb-3 text-center">{productName}</Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price: ₱ {price}</Card.Subtitle>
		        <Card.Text></Card.Text>
		        
		        <Link className="btn btn-primary mx-3" to={`/updateProduct/${_id}`}>Update</Link>

		    {
		    	(isActive)?
		        <Link className="btn btn-danger" to={`/isActive/${_id}`}>Disable</Link>
		        :
		        <Link className="btn btn-danger" to={`/isActive/${_id}`}>Enable</Link>
		    }

		         
		        

		    </Card.Body>
		</Card>

		)


}