import {useEffect, useContext} from 'react';
import {Navigate} from  'react-router-dom'; //use to navigate pages
import UserContext from '../UserContext';


export default function Logout() {
	
	//Consume the UserContext object and desctructure it to access the user state and unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext);


	unsetUser(); //clear localStorage


	//localStorage.clear();	//to delete token when logging out


	useEffect(() => {

		setUser({id:null}) //This is to allow us to set the user stat back to its original value
	})

	return(

		<Navigate to="/login"/>

		)
}

