import AllProduct from '../components/AllProduct.js';
import AddProduct from '../components/AddProduct.js';
import UpdateProduct from '../components/UpdateProduct.js'
import {Link} from 'react-router-dom'

import {useState, useEffect} from 'react'

 
export default function Admin()  {

	const [products, setProducts] = useState([]);		
 
		useEffect(() => {	
			 fetch(`${process.env.REACT_APP_API_URL}/products/all`)
			 .then(res => res.json())
			 .then(data => {

			 	console.log(data);

			 	setProducts(data.map(product => {
			 		return (
			 			
			 			<AllProduct key={product._id} productProps={product}/>
				 		
			 			)
			 		}))

			 	})
		}, [])



	return (
		<>
			<Link  to="/admin/addProduct">AddProduct</Link>
			<AddProduct/>
			<h1 className="my-5 text-center">List of Products</h1>
			{products}
		</>

		

		)
}